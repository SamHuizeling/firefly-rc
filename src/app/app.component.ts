import {Component, ViewChild, OnInit} from '@angular/core';
import {StatusBar, Deeplinks, Splashscreen} from 'ionic-native';
import {Platform, Nav, MenuController} from 'ionic-angular';

// IMPORT PAGES
import { Tickets, Account, About, Feedback, Planner} from '../pages';

// IMPORT SERVICES
import {DatabaseService} from '../services/database.service';
import {HttpService} from '../services/http.service';
import {TicketsService} from '../pages/tickets/tickets.service';
import {LocationService} from '../services/location.service';


@Component({
  template: `<ion-nav [root]="rootPage"></ion-nav>`
})

export class FireFly implements OnInit{
 @ViewChild(Nav) nav: Nav;

  // make PlannerPage the root (or first) page
  rootPage: any = Planner;
  pages: Array<{title: string, component: any}>;

  constructor(public locationService: LocationService,
              public databaseService: DatabaseService,
              public httpService: HttpService,
              public platform: Platform,
              public menu: MenuController) {

    // CALL SERVICES SO THE ARE NOT UNUSED
    this.httpService = httpService;
    this.databaseService = databaseService;
    this.locationService = locationService;

    this.initializeApp();


    this.pages = [
      {title: 'Tickets', component: Tickets},
      {title: 'Account', component: Account},
      {title: 'About', component: About},
      {title: 'Feedback', component: Feedback}
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      Splashscreen.hide();
      this.httpService.authenticate();
    });
  }

  ngOnInit() {
    console.log("INIT APP");
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
      // setup deeplinking according to http://blog.ionic.io/deeplinking-in-ionic-apps/
      // cordova plugin add ionic-plugin-deeplinks --variable URL_SCHEME=firefly --variable DEEPLINK_SCHEME=https --variable DEEPLINK_HOST=firefly-backend.herokuapp.com
      Deeplinks.routeWithNavController(this.nav,
      {
        '/Ticket/:ticketId': Tickets
      }).subscribe((match) => {
        console.info('Deeplink navigate to', match.$link.path);
        console.log('Successfully routed', match);
      }, (nomatch) => {
        console.warn('Deeplink not matched', nomatch);
      });
    });
  };

  openPage(page) {
    this.menu.close();
    this.nav.push(page.component);
  }
};