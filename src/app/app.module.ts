import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicApp, IonicModule, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { FireFly } from './app.component';
import { DatabaseService } from './services/database.service';
import { HttpService } from './services/http.service';
import { TicketsService } from './pages/tickets/tickets.service';
import { LocationService } from './services/location.service';

import { RoundTime } from './planner.pipes';
import { MapComponent } from '../../components/map/map';
import { AuthenticationComponent } from '../../components/authentication/authentication';
import { ItineraryComponent } from '../../components/itineray/itineray';
import { PaymentComponent } from '../../components/payment/payment';


// IMPORT PAGES
import { Planner } from './pages/planner/planner';
import { Account } from './pages/account/account';
import { Tickets } from './pages/tickets/tickets';
import { About } from './pages/about/about';
import { Feedback } from './pages/feedback/feedback';

@NgModule({
  declarations: [
    FireFly,
    Planner,
    Tickets,
    About,
    Feedback,
    MapComponent,
    RoundTime
  ],
  imports: [
    IonicModule.forRoot(FireFly),
    FormsModule
  ],
  entryComponents: [  
    FireFly,
    Planner,
    Tickets,
    About,
    Feedback,
    MapComponent,
    RoundTime ],
  bootstrap: [IonicApp],
  providers: [DatabaseService, HttpService, TicketsService, LocationService, Storage ]
})
export class AppModule {}
