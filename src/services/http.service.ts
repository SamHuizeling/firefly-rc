import {Http, Headers, RequestOptions, RequestOptionsArgs, Response} from '@angular/http';
import {DatabaseService, Tokens} from './database.service';
import {Injectable, OnInit} from '@angular/core';
import {Endpoint} from '../constants';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';


@Injectable()
export class HttpService {
  is_authenticated: boolean;
  access_token: string;
  profile: any;

  constructor(private http: Http,
              private databaseService: DatabaseService) {
    this.is_authenticated = false;
    this.access_token = '';
    this.profile = {};
  }

  _transformRequest(object) {
    var str = [];
    for (var key in object) {
      if (object.hasOwnProperty(key)) {
        str.push(key + '=' + object[key]);
      }
    }
    return str.join("&");
  }

  authenticate() {
    console.log('authenticating');
    this.databaseService.getTokens()
      .filter((tokens) => tokens != null)   // stop the stream if the database returns null
      .map(tokens => this.access_token = tokens.access_token)  // store the access token
      .flatMap(res => this.getProfile())
      .catch((err) => this.loginFailure(err))  // catch login errors (remove tokens from DB)
      .subscribe(res => {
      }, err => console.log("Something went wrong:", err));
  }

  loginFailure(error: any) {
    // In a real world app, we might send the error to remote logging infrastructure
    let errMsg = error.message || 'Server error';
    // console.log('login failure' + errMsg); // log to console instead
    console.log('login failure ' + JSON.stringify(error, null, 2));
    console.log('removing tokens from storage');
    this.databaseService.removeTokens().subscribe();
    return Observable.throw(errMsg);
  }

  update_or_create_tokens(username = null, password = null, refresh_token = null) {
    let grant_type = username !== null && password !== null ? 'password' : 'refresh_token';

    let payload = {
      grant_type: grant_type,
      client_id: Endpoint.client_id,
      client_secret: Endpoint.client_secret,
    };

    if (grant_type === 'password') {
      payload['username'] = username;
      payload['password'] = password;
    } else if (grant_type === 'refresh_token') {
      payload['refresh_token'] = refresh_token;
    }

    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let options = new RequestOptions({headers: headers});

    return this.http.post(Endpoint.url + '/auth/token/', this._transformRequest(payload), options)
      .map((res: Response) => res.json())
      .map(tokens => {
        this.access_token = tokens.access_token;
        return tokens;
      })
      .flatMap(tokens => this.databaseService.storeTokens(tokens));
  }


  private handleError(error: any) {
    // In a real world app, we might send the error to remote logging infrastructure
    let errMsg = error.message || 'Server error';
    // console.log('login failure' + errMsg); // log to console instead
    console.log('login failure ' + JSON.stringify(error, null, 2));
    return Observable.throw(errMsg);
  }

  getProfile(): Observable<Response> {
    return this.get('/accounts/profile.json').map(
      res => {
        this.profile = res.json();
        this.is_authenticated = true;
        return res;
      }
    );
  }

  get_ext(url): Observable<Response> {
    return this.http.get(url);
  }

  get(url, headers = null, auth = true) {
    let requestHeaders = new Headers();
    requestHeaders.append('Content-Type', 'application/json');

    if (auth) {
      if (this.access_token !== '') {
        requestHeaders.append('Authorization', 'Bearer ' + this.access_token);
      }
    }

    if (headers !== null) {
      for (var key in headers) {
        if (headers.hasOwnProperty(key)) {
          requestHeaders.append(key, headers[key]);
        }
      }
    }

    return this.http.get(Endpoint.url + url, new RequestOptions({headers: requestHeaders}));
  }

  post(url, payload, headers = null) {
    let requestHeaders = new Headers();
    requestHeaders.append('Content-Type', 'application/json');

    if (this.access_token !== '') {
      requestHeaders.append('Authorization', 'Bearer ' + this.access_token);
    }

    if (headers !== null) {
      for (var key in headers) {
        if (headers.hasOwnProperty(key)) {
          requestHeaders.append(key, headers[key]);
        }
      }
    }
    return this.http.post(Endpoint.url + url, JSON.stringify(payload), new RequestOptions({headers: requestHeaders}));
  }

  delete(url, headers = null) {
    let requestHeaders = new Headers();
    requestHeaders.append('Content-Type', 'application/json');

    if (this.access_token !== '') {
      requestHeaders.append('Authorization', 'Bearer ' + this.access_token);
    }

    if (headers !== null) {
      for (var key in headers) {
        if (headers.hasOwnProperty(key)) {
          requestHeaders.append(key, headers[key]);
        }
      }
    }

    return this.http.delete(Endpoint.url + url, new RequestOptions({headers: requestHeaders}));
  }


}
