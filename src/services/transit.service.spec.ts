/*import {beforeEach, beforeEachProviders, describe, expect, it} from '@angular/core/testing';
import {asyncCallbackFactory, injectAsyncWrapper, providers}   from '../../test/diExports';
import {Geoposition} from '../../node_modules/ionic-native/dist/plugins/geolocation.d';
import {inject, async, fakeAsync, tick} from '@angular/core/testing';
import {Response, ResponseOptions, Http} from '@angular/http';
import {DatabaseService, Tokens} from './database.service';
import {LocationService} from './location.service';
import {TransitService} from './transit.service';
import {HttpService} from './http.service';
import {Observable} from 'rxjs/Observable';
import {Geolocation} from 'ionic-native';
import {provide} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/observable/of';

class MockHttpService extends HttpService {
  post(url, payload, headers = null): Observable<Response> {
    console.log("post received");

    let options = new ResponseOptions({
      body: {message: 'yeah baby yeah'}
    });
    let res = new Response(options);
    return Observable.of(res);
  }
}

class MockLocationService {
  geoLoc(): Observable<any> {
    return Observable.timer(100, 100)
      .map(() => {
        console.log("gps update :)");
        return {position: {coords: {latitude: 1.1, longitude: 2.2}}};
      });
  }
}

describe('transit_Service: now for real', () => {

  let httpSevice;
  let location;
  let transit;

  // setup
  beforeEachProviders(() => [
    provide('HttpService', {useClass: MockHttpService}),
    provide('LocationService', {useClass: MockLocationService}),
    TransitService
  ]);

  beforeEach(inject([TransitService], (_transit) =>
    it('should return available languages', done => {
      expect(true).toBe(false);
      console.log('running awesome test');
      transit.check_status(1)
        .subscribe(x => {
          console.log(x);
        });
    })));
});
*/