import {Geoposition, GeolocationOptions} from '../../node_modules/ionic-native/dist/plugins/geolocation.d';
import {ReplaySubject} from 'rxjs/ReplaySubject';
import {HttpService} from './http.service';
import {Observable} from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import {Geolocation} from 'ionic-native';
import {Platform} from 'ionic-angular';
import {Subject} from 'rxjs/Subject';


export class LatLon {
  lat: number;
  lon: number;
}

declare var google;

@Injectable()
export class LocationService {
  geoLocation = new ReplaySubject<Geoposition>(1);
  lastLocation: Geoposition;

  constructor(private httpService: HttpService,
              private platform: Platform) {
    this.platform.ready().then(() => {
      this.init();
    });
  }

  init() {
    let options: GeolocationOptions = {maximumAge: 10000, timeout: 300000, enableHighAccuracy: true};
    Geolocation.watchPosition(options)
      .subscribe((pos) => {
          this.geoLocation.next(pos);
          // console.log('new position:', pos);
          this.lastLocation = pos;
        }
        // , (err) => {
        //   let alert = this.alertController.create({
        //     title: 'No Gps location found',
        //     buttons: ['OK']
        //   });
        //   alert.present();
        // }
        // }
      );
  }

  geoLoc(): Observable < Geoposition > {
    return Geolocation.watchPosition();
  }

  // getLocationText() {
  //   // !!!!Beware don't subscribe to this function, because it will drain the number of remaining
  //   // google api calls!!!!!
  //   // instead use getLocationText().first().subscribe(func);
  //   return this.geoLocation
  //     .flatMap(pos => {
  //       let lon = pos.coords.longitude;
  //       let lat = pos.coords.latitude;
  //       console.log('beware: hitting google api!');
  //       return this.httpService.get_ext("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&key=AIzaSyAvr1gaxFxuc-Iptas2PGiYCUxPsQZ8AxY");
  //     })
  //     .map((res) => res.json().results[0].formatted_address);
  // }
}
