import {Storage} from '@ionic/storage';
import {Observable}   from 'rxjs/Observable';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';


export class Tokens {
  refresh_token: string;
  scope: string;
  token_type: string;
  access_token: string;
  expires_in: number;
}

@Injectable()
export class DatabaseService {
  tokens: Tokens;

  constructor(public storage: Storage) {
  }

  get(key: string) {
    return Observable.fromPromise(this.storage.get(key)).map(res => JSON.parse(res));
  }

  set(key: string, value: any) {
    return this.storage.set(key, JSON.stringify(value));
  }

  removeTokens() {
    return Observable.fromPromise(this.storage.remove('tokens')).map(res => {
      this.tokens = new Tokens;
      return res;
    });
  }

  getTokens() {
    return Observable.fromPromise(this.storage.get('tokens'))
      .map(tokens => {
        if (tokens != null) {
          this.tokens = JSON.parse(tokens);
          console.log('found tokens in storage:', JSON.stringify(this.tokens));
          console.log('this.tokens.access_token=' + this.tokens.access_token);
          return this.tokens;
        } else {
          console.log('no tokens found in db');
          return null;
        }
      }, error => {
        console.log('something went wrong' + error);
      });
  }

  storeTokens(tokens) {
    this.tokens = tokens;
    console.log('received token saving now.');
    return Observable.fromPromise(this.storage.set('tokens', JSON.stringify(this.tokens, null, 2))).map(
      data => console.log('token saved'), error => console.log('saving failed: ' + error)
    );
  }
}
