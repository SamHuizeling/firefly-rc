import {ViewController} from 'ionic-angular';
import {Component, OnInit} from '@angular/core';
import {transitModes} from '../../utils/journey.utils';
import {TicketsService} from '../tickets/tickets.service';

@Component({
  templateUrl: 'history.page.html'
})

export class History {
  historyList: string[];

  constructor(private viewCtrl: ViewController,
              private ticketsService: TicketsService) {
  }

  ngOnInit() {
    this.ticketsService.getHistoryPlan();
    this.historyList = this.ticketsService.history;
  }
}