import {Injectable, OnInit} from '@angular/core';
import {Subject}    from 'rxjs/Subject';
import {HttpService} from '../../services/http.service';
import {Events} from 'ionic-angular';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import {Endpoint} from "../../constants";
import 'rxjs/Rx';

declare var google;

export class JourneyRequest {
  origin: string;
  destination: string;
}

@Injectable()
export class PlannerService {
  private places: string[];
  journeyRequest: JourneyRequest;
  private journeySource = new Subject<Object>();
  public journey$ = this.journeySource.asObservable();
  autocomplete: Observable<Array<string>>;

  public recentPlaces = [
    'Amsterdam Centraal',
    'Utrecht Centraal',
    'Rijksmuseum, Museumstraat, Amsterdam',
    'Olympisch stadion, Amsterdam'
  ];

  constructor(private httpService: HttpService,
              private http: Http,
              public events: Events) {

    this.journeyRequest = {
      origin: 'Schiphol',
      destination: 'Destination'
    };
  }

  autoComplete(query) {
    // straight get from backend without authentication
    return this.http
      .get(Endpoint.url + '/autocomplete/' + query + '/request.json')
      // .do(res=>console.log(res.json()))
      .map(request => request.json()['result'])
      .catch(this.handleError); // todo: doesn't seem to work on iphone
  }

  handleError(error) {
    // error handling callback function of autocomplete, this will fire
    console.log('error on search');
    console.log(error);
    return Observable.throw(error.json().error || 'server error');
  }

  requestJourney(): Observable<boolean> {
    var body = {origin: this.journeyRequest.origin, destination: this.journeyRequest.destination};
    console.log(body);
    return this.httpService.post('/journey/request/', body)
      .map((journey) => journey.json())
      .map((journey) => {
        return this.journeyFound(journey);
      }); // todo: make journey not so special. Uber and other options are important too.
  }

  journeyFound(journey): boolean {
    let journeyFound: boolean = false;
    console.log('journey_:', journey);
    if (_.isEmpty(journey['plan'])) {
      console.log('no journey found');
      return false;
    } else {
      this.journeySource.next(journey);
      return true;
    }
  }
}
