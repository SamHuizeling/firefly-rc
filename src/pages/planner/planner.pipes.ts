import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'roundtime'
})

export class RoundTime implements PipeTransform {
  transform(value: number) {
    return Math.round(value);
  }
}
