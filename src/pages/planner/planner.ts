import { AlertController, NavController, MenuController, LoadingController, ToastController } from 'ionic-angular';
import { transitModes, journeyInstructions } from '../../utils/journey.utils';
import { JourneyRequest, PlannerService } from './planner.service';
import { LocationService } from '../../services/location.service';
import { MapComponent } from '../../components/map/map';
import { MapService } from '../../components/map/map.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { timer } from 'rxjs/observable/timer';
import { TicketsService } from '../tickets/tickets.service';
import { HttpService } from '../../services/http.service';
import { handleError, doAlert } from '../../utils/interaction';
import { RoundTime } from './planner.pipes';
import { Quotation } from '../quotation/quotation';
import { Observable } from "rxjs";
import { Diagnostic } from 'ionic-native';

// todo: rename journey to transit

@Component({
  templateUrl: 'planner.html',
})

export class Planner {
  journeyRequest: JourneyRequest;
  journeySubscription: Subscription;
  currentLocation: string;
  yourLocation: string;
  yourLocationText: string = "Current location"
  modalityDetails: Object;
  is_journey: Boolean;
  journey: Object;
  planForm: any;
  passengers: number;
  selectedDestination: string = "";
  map: any;
  lat: number = null;
  lon: number = null;

  public mapName: string = "planner";
  currentLocationQueried: boolean = false;  // only once for now. We cannot do this with each gps update
  uber_low: number;
  uber_high: number;
  uber_estimation: number;
  transitModesList: string[];
  customLocation: boolean = false;
  listPredictions: string[];
  originPredictions: string[];
  originChange: boolean;

  constructor(private alertController: AlertController,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private menu: MenuController,
    private plannerService: PlannerService,
    private form: FormBuilder,
    public httpService: HttpService,
    public mapService: MapService,
    public ticketsService: TicketsService,
    private nav: NavController,
    private locationService: LocationService) {

    this.menu.swipeEnable(true);
    this.currentLocation = 'Looking for location';
    this.is_journey = false;
    this.planForm = this.form.group({
      origin: "",
      destination: ""
    });

    this.modalityDetails = {
      transit: false,
      uber: false
    };
    this.passengers = 1;
    this.originChange = false;
  }


  gpsErrorCallback(error) { console.log(error) }


  ngOnInit() {
    // for some reason this value is not stored at definition:
    this.mapName = 'planner';
    console.log("OnInit planner", this.mapName);

    // get first gps update transformed to text:
    // (if we dont do first(), we deplete google location api!! )
    // this.locationService.getLocationText().subscribe(loc => this.currentLocation = loc);
    // this.locationService.getLocationText().subscribe(loc => this.yourLocation = loc);
    this.locationService.geoLocation.first().subscribe(loc => this.currentLocation = this.yourLocationText);


    let gpsSuccesCallback = (isAvailable) => {
      if (isAvailable) {
        console.log('gps operational');
      } else {
        let gpsAuthorization = Diagnostic.isLocationAuthorized();
        console.log('gps authorizations=', gpsAuthorization);
        doAlert(this.alertController, "please enable your gps");
      }
    }

    timer(500)
      .subscribe(res => Diagnostic.isGpsLocationEnabled().then(gpsSuccesCallback).catch(this.gpsErrorCallback));

    // subscribe to journey results:
    this.journeySubscription = this.plannerService.journey$.subscribe(
      journey => {
        this.journey = journey;
        console.log("journey:", journey);
        this.mapService.addPolyline(journey);
        this.is_journey = true;
        this.calcUberPrice();
        this.transitModesList = transitModes(journey['plan']);
        console.log('transitModes', this.transitModesList);
      },
      err => {
        console.log(err);
      });
  }


  onMap() {
    console.log('map clicked');
  }

  pleaseShowMap() {
    timer(200).subscribe(res => this.mapService.resizeEvent());
    timer(2000)
      .flatMap(() => this.mapService.centerOnce())
      .subscribe();
  }

  onClear() {
    console.log('clear pressed');
    this.is_journey = false;
    this.mapService.removePolyLines();
    this.listPredictions = [];
    this.originPredictions = [];
  }

  onSearchBarClick() {
    // todo: for now use is_journey to toggle the travel-options in the bottem
    // better would be to have a dedicated flag (showTravelOptions)
    this.pleaseShowMap();
    this.is_journey = false;
    this.mapService.removePolyLines();
    // this.mapService.onCenterLocation();
  }

  focusOrigin(input) {
    this.pleaseShowMap();
    this.currentLocation = '';
    input.setFocus();
  }

  /*    changeOrigin() {
      this.customLocation = !this.customLocation;
      if (this.customLocation) {
        this.currentLocation = this.location_hack;
      } else {
        this.locationService.getLocationText().first().subscribe(loc => this.currentLocation = loc);
      } */

  changeOrigin(originQuery) {
    this.originChange = true;

    this.currentLocation = originQuery;

    this.plannerService.autoComplete(originQuery).subscribe(result => {
      console.log('component found data');
      console.log(result);
      this.originPredictions = result;
    }, error => console.log(error));

    // this.customLocation = !this.customLocation;
    // if (this.customLocation) {
    //   this.currentLocation = '';
    // } else {
    //   this.locationService.getLocationText().first().subscribe(loc => this.currentLocation = loc);
    // }
  }

  onMyLocation() {
    this.originChange = false; // check this!
    this.currentLocation = this.yourLocationText;
  }

  doSearch(ev) {
    // set val to the value of the searchbar
    if (ev.target.value) {
      let val = ev.target.value;

      // fire a resize event, else the map will be blank:
      this.mapService.resizeEvent();

      // if the value is an empty string don't filter the items
      if (val.length > 0) {

        // subscribe to the autocomplete function which returns an http get request to the backend microservice
        this.plannerService.autoComplete(val).subscribe(result => {
          console.log('component found data');
          console.log(result);
          this.listPredictions = result;
        }, error => console.log(error));
      }
    }
  }

  calcUberPrice() {
    if (this.journey['plan']['uber']) {
      this.uber_low = this.journey['plan']['uber']['response']['low_estimate'] * (Math.floor((this.passengers - 1) / 4) + 1);
      this.uber_high = this.journey['plan']['uber']['response']['high_estimate'] * (Math.floor((this.passengers - 1) / 4) + 1);
      this.uber_estimation = (this.uber_low + this.uber_high) / 2;
    }
  }


  onEnterKey(event) {
    console.log("enter key pressed!", event);
    let place = event.target.value;
    this.onSelectDestination(place);
  }

  onSelectOrigin(origin = null) {
    if (!origin) {
      origin = this.yourLocationText;
    }
    console.log(origin);
    this.currentLocation = origin;
    this.originChange = false;
    if (this.plannerService.journeyRequest.destination != 'Destination') {
      this.onClear();
      this.setCurrentLocation();
      this.planJourney();
    }
  }

  planJourney() {
    let spinner = this.loadingController.create({ content: "Creating a plan" });
    spinner.present();

    let journeyFound: boolean = false;
    this.plannerService.requestJourney()
      .do((foundJourney) => journeyFound = foundJourney) // save foundJourney
      .flatMap(() => Observable.fromPromise(spinner.dismiss()))
      .subscribe(() => {
        console.log('journeyFound', journeyFound);
        if (!journeyFound) {
          let toast = this.toastController.create({
            message: "Sorry, can't help you. Couldn't find anything :/",
            duration: 2000,
            position: 'middle'
          });
          toast.present();
        }
      },
      err => {
        console.log(err);
        handleError(this.alertController, err, "onSelectDestination", spinner);
      });
  }

  setCurrentLocation(){
      // Todo: Check logics and add autocomplete to the origin
    if (this.currentLocation != this.yourLocationText) {
      this.plannerService.journeyRequest.origin = this.currentLocation;
    } else {
      let origin: string;
      // 41.43206,-81.38992
      origin = this.locationService.lastLocation.coords.latitude.toString() +
        "," + this.locationService.lastLocation.coords.longitude.toString();
      this.plannerService.journeyRequest.origin = origin;
    }
  }

  onSelectDestination(place) {
    this.setCurrentLocation();

    this.listPredictions = [];
    this.selectedDestination = place;
    this.plannerService.journeyRequest.destination = place;
    this.planJourney();
  }

  checkInTransit() {
    console.log("PLANNER: Send journey to quotation ", this.journey);
    this.nav.push(Quotation, { 'plan': this.journey });

    // Todo: Move logic to quotation.ts
    // let spinner = this.loadingController.create({content: "Confirming your journey"});
    // spinner.present();

    // checkin with post
    // var body = {id: this.journey['request']['id'], n_persons: this.passengers};
    // this.httpService.post('/journey/confirm.json', body)
    //   .map((journey) => this.ticketsService.ticket = journey.json())
    //   .flatMap(() => this.httpService.getProfile())
    //   .flatMap(() => Observable.fromPromise(spinner.dismiss()))
    //   .catch((err) => handleError(this.alertController, err, 'checkInTransit', spinner))
    //   .subscribe(
    //     journey => {
    //       this.nav.push(Quotation, {'plan': this.journey});
    //     },
    //     err => console.log(err));
  }

  journeyDetails(event) {
    for (var details in this.modalityDetails) {
      if (details === event) {
        this.modalityDetails[details] = this.modalityDetails[details] ? false : true;
      } else {
        this.modalityDetails[details] = false;
      }
    }
  }

  showTravelDetails() {
    console.log(journeyInstructions(this.journey['plan']));
    let alert = this.alertController.create({
      title: 'Travel instructions',
      subTitle: journeyInstructions(this.journey['plan']),
      buttons: ['OK']
    });
    alert.present();
  }

  onUber() {
    let alert = this.alertController.create({
      title: 'Uber deeplink will be available soon!',
      buttons: ['OK']
    });
    alert.present();
  }

  onAbel() {
    let alert = this.alertController.create({
      title: 'Abel deeplink will be available soon!',
      buttons: ['OK']
    });
    alert.present();
  }



  confirmCheckIn() {
    let price: number = this.journey['plan']['transit']['total_fare_plan']['fare'];

    if (!this.httpService.is_authenticated) {
      let alert = this.alertController.create({
        title: 'You are not logged in',
        subTitle: 'Please login first',
        buttons: ['OK']
      });
      alert.present();
    } else {
      this.checkInTransit();
    }
  }
}
