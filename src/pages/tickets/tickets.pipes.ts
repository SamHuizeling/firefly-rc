import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'datetotime'
})

export class DateTimePipe implements PipeTransform {
  transform(value: string) {
    let time_array = value.split('T')[1].split('Z')[0].split(':');
    return time_array[0] + ':' + time_array[1];
  }
}
