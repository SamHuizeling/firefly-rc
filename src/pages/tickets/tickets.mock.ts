export let tickets = [
  {
    "id": 1,
    "journey_request": {
      "id": 1,
      "origin": "schiphol",
      "destination": "rijksmuseum",
      "timestamp": "2016-07-14T11:18:36.547008Z",
      "user": 1,
      "origin_lat": 52.307707,
      "origin_lng": 4.7645433,
      "destination_lat": 52.360154,
      "destination_lng": 4.883353,
      "transit_status": "OK",
      "transit_distance": 14925,
      "transit_duration": 2274,
      "uber_status": "200",
      "checkin_possible": true,
      "agency": "Connexxion",
      "line": "197",
      "line_name": "Schiphol P30 - Amsterdam Leidseplein",
      "uber_low_estimate": 22,
      "uber_high_estimate": 22,
      "uber_currency": null,
      "uber_distance": 9300,
      "uber_duration": 1320,
      "plan_file": "/media/journey_requests/20160714111836_51.json"
    },
    "user": 1,
    "journey_code": "3084",
    "n_persons": 1,
    "date_created": "2016-07-14T11:19:12.840009Z",
    "origin": "schiphol",
    "destination": "rijksmuseum",
    "transit_distance": 14925,
    "transit_duration": 2274,
    "transit_fare": "15.82",
    "agency": "Connexxion",
    "line": "197",
    "line_name": "Schiphol P30 - Amsterdam Leidseplein",
    "is_visible": true,
    "current_state": "active",
    "state_history": [
      {
        "date": "2016-07-14T11:19:12.842794Z",
        "state": "requested"
      },
      {
        "date": "2016-07-14T11:19:14.445143Z",
        "state": "active"
      }
    ]
  }
];

export let journey = {
  "plan": {
    "uber": {
      "status_code": 200,
      "response": {
        "currency_code": "EUR",
        "surge_multiplier": 1,
        "high_estimate": 22,
        "product_id": "98ea6c00-6408-430b-b2f0-3ff3a8b87d00",
        "display_name": "uberX",
        "minimum": 5,
        "distance": 9.3,
        "estimate": "€22",
        "localized_display_name": "uberX",
        "duration": 1320,
        "low_estimate": 22
      }
    },
    "transit": {
      "total_fare_plan": {
        "line_name": "Schiphol P30 - Amsterdam Leidseplein",
        "line": "197",
        "agency": "Connexxion",
        "checkin_possible": true,
        "fare": 15.817
      },
      "status_code": 200,
      "response": {
        "status": "OK",
        "routes": [
          {
            "summary": "",
            "copyrights": "Map data ©2016 Google",
            "bounds": {
              "northeast": {
                "lat": 52.360154,
                "lng": 4.88356
              },
              "southwest": {
                "lat": 52.3075245,
                "lng": 4.75075
              }
            },
            "waypoint_order": [],
            "legs": [
              {
                "departure_time": {
                  "text": "1:19pm",
                  "value": 1468495146,
                  "time_zone": "Europe/Amsterdam"
                },
                "start_address": "Amsterdam Airport Schiphol, Netherlands",
                "duration": {
                  "text": "38 mins",
                  "value": 2274
                },
                "start_location": {
                  "lat": 52.307707,
                  "lng": 4.7645433
                },
                "distance": {
                  "text": "14.9 km",
                  "value": 14925
                },
                "via_waypoint": [],
                "end_location": {
                  "lat": 52.360154,
                  "lng": 4.883353
                },
                "steps": [
                  {
                    "fare_plan": null,
                    "duration": {
                      "text": "1 min",
                      "value": 76
                    },
                    "steps": [
                      {
                        "duration": {
                          "text": "1 min",
                          "value": 61
                        },
                        "start_location": {
                          "lat": 52.307707,
                          "lng": 4.7645433
                        },
                        "distance": {
                          "text": "85 m",
                          "value": 85
                        },
                        "end_location": {
                          "lat": 52.30770589999999,
                          "lng": 4.7632939
                        },
                        "polyline": {
                          "points": "ejg~Hkqa\\@jDAlA"
                        },
                        "html_instructions": "Head <b>west</b> on <b>Inspecteurspassage</b> toward <b>Havenmeesterweg</b>",
                        "travel_mode": "WALKING"
                      },
                      {
                        "duration": {
                          "text": "1 min",
                          "value": 15
                        },
                        "start_location": {
                          "lat": 52.30770589999999,
                          "lng": 4.7632939
                        },
                        "maneuver": "turn-left",
                        "distance": {
                          "text": "23 m",
                          "value": 23
                        },
                        "end_location": {
                          "lat": 52.307553,
                          "lng": 4.76361
                        },
                        "polyline": {
                          "points": "ejg~Hqia\\VULGEa@"
                        },
                        "html_instructions": "Turn <b>left</b> onto <b>Havenmeesterweg</b><div style=\"font-size:0.9em\">Destination will be on the left</div>",
                        "travel_mode": "WALKING"
                      }
                    ],
                    "start_location": {
                      "lat": 52.307707,
                      "lng": 4.7645433
                    },
                    "distance": {
                      "text": "0.1 km",
                      "value": 108
                    },
                    "end_location": {
                      "lat": 52.307553,
                      "lng": 4.76361
                    },
                    "polyline": {
                      "points": "ejg~Hkqa\\@jDAlAVULGEa@"
                    },
                    "html_instructions": "Walk to Schiphol, Skyport",
                    "travel_mode": "WALKING"
                  },
                  {
                    "transit_details": {
                      "departure_time": {
                        "text": "1:21pm",
                        "value": 1468495260,
                        "time_zone": "Europe/Amsterdam"
                      },
                      "line": {
                        "vehicle": {
                          "name": "Bus",
                          "type": "BUS",
                          "icon": "//maps.gstatic.com/mapfiles/transit/iw2/6/bus2.png"
                        },
                        "name": "Schiphol P30 - Amsterdam Leidseplein",
                        "url": "http://www.connexxion.nl/dienstregeling/lijn?ID=M197",
                        "short_name": "197",
                        "agencies": [
                          {
                            "name": "Connexxion",
                            "url": "http://www.connexxion.nl/",
                            "phone": "011 31 900 2666399"
                          }
                        ]
                      },
                      "num_stops": 13,
                      "departure_stop": {
                        "name": "Schiphol, Skyport",
                        "location": {
                          "lat": 52.307553,
                          "lng": 4.76361
                        }
                      },
                      "arrival_time": {
                        "text": "1:57pm",
                        "value": 1468497420,
                        "time_zone": "Europe/Amsterdam"
                      },
                      "headsign": "Leidseplein Amsterdam",
                      "arrival_stop": {
                        "name": "Amsterdam, Rijksmuseum",
                        "location": {
                          "lat": 52.360154,
                          "lng": 4.883353
                        }
                      }
                    },
                    "duration": {
                      "text": "36 mins",
                      "value": 2160
                    },
                    "start_location": {
                      "lat": 52.307553,
                      "lng": 4.76361
                    },
                    "fare_plan": {
                      "line_name": "Schiphol P30 - Amsterdam Leidseplein",
                      "line": "197",
                      "agency": "Connexxion",
                      "checkin_possible": true,
                      "fare": 15.817
                    },
                    "distance": {
                      "text": "14.8 km",
                      "value": 14817
                    },
                    "end_location": {
                      "lat": 52.360154,
                      "lng": 4.883353
                    },
                    "polyline": {
                      "points": "eig~Hqka\\IPIDIJUb@Qf@IN?@@DOLOf@IRIjA?A_CnCm@b@kA~AGFCNELAR?fC?X@j@?dAEd@CVAJEPEPGNCNCJC^ALAVA^Bd@Bd@F\\FTJZ~ArFf@pBLj@H`@Dh@B^?P?@BRDpADfCCf@C`@CZSv@e@|A[|@Yf@W^W\\[X_@Vi@Pe@Hi@B_@E[G]Mi@W_@ScAc@}@YYGwA[YIqCk@]MoAg@kAo@k@]y@g@_By@qDcCmA{@gA_Ay@u@YWyBiCc@i@a@c@_@k@g@y@Ye@OWc@o@KUa@o@KWK_@Ia@Ea@Ca@Ao@Ae@?m@BcAB}@L}@NmAT}ANoALwABcA@q@Cy@IaGGmEMyJMyLGgFEqCGsDAg@?ICMGSc@oAa@eAYs@o@{@oB_CmCyCw@P{@gAUKEsAoBsBqA_B]_@a@e@U[KQQ[i@iA]_Aa@yAACGs@QmAIcACiA?q@Es@?iB?eBC}C@{A?}@?aACo@?g@CWJ_ADc@Ji@@G@?Po@\\}@Xm@b@gALBf@gBxB_F^{@f@kAfAeCp@eBB]DiBAiACiCEeDC_ACWWqAaAkB?EAEYg@GLi@u@g@y@KS]k@JUO[mAgBe@{@vGiOJKZq@FMDKBEBO@K@ICUAECGIKGKQYMSGEe@s@[e@MYAAMQi@y@Si@w@{@KQQYqBgDmA}BOYSa@IQQ_@KSEMGIEEIAGAM@EDIJMV{@zAg@v@y@zA_@j@]h@[f@OPKJWNQFSDMBM?OAKCYK]SQSOMi@q@AAe@q@a@g@U_@c@q@]k@_AyBaGmKwAgCaBwCAGi@eAKQKMaAuAeBgDkA}Ba@y@S_@Ws@Wq@KYKKKGMEMAAw@ASCQ]eAa@sAsDuJ{@_C_@eAEOEOAQ?MSc@KY?AYu@Qg@Qc@EEECOEYECVCXI\\IRUVYV{@p@UNEJMLG@QJGBGAE?CEGIIIEQ]gCAIGk@Gw@MoAKyAEk@KkAE_AIoAE}AG{HQeA?mE?K@E?Q?Y?S?W?Q?[?E?c@?c@@O?]?E?K?E@s@?o@?E?I?E?Q?K?E?K?W?O?Q@wA@eA?c@?Q?eA?Y@[?a@?k@?I?O@Q?QAeA?Q@s@?o@?E?]?U?Q@_@?S?Q?Q?QAQ?E@K?C?K?C?W?G?Q?Q?a@@Q?c@?O?Q?Q?s@?Q?Q?Q@iB?O?c@?_H@kB?_A?Q?E?K?E?]?E?I@E?]?Q@Q?O@o@?c@Aa@@c@@W@YBy@?q@Bc@FkADsADo@DeAD{@Fm@@OLyANaBDg@Fe@PiBLkAHu@PkBLsAHy@Dm@B]Bq@F}@Bs@Bi@Bs@@gA?_@@q@?mA?}AAk@?e@C}@AcAEcBAg@Ae@CaA?MAQ?AASAc@Ak@AYAk@AS?O?KI}CCeAAc@?Y?BEsCAUG_D?k@?AA_@Cq@MmFIsA?mACsBAu@KuECk@j@EEiDAcBo@Aq@UIRi@A{@D_@?G?W?KBMQIGGKCECGu@?G?i@?Q?k@@g@CkBKWCWG]EQCg@Ca@BQDcBf@K@c@Jc@He@HkAXu@PQHu@AERaAZgA^SHI@I?E?KBEAKECEEICEEMEWCYCa@GoAAKAKICICQ?C?MBK@K?s@FgB?uBF{@?s@Bq@?eCJ]?[@U@q@DQ?KLGMO?GAYCAGCMCCEKIGGEC?IEI?IBIDIFSe@GSI[Ko@i@cD_@qBYoBMaAGWUeAWcBWwAO{@Ko@Ga@G_@Ko@Ie@Q_AWwAEYMq@Kc@}@kFcAsGUyAYoBe@yDO{@K[COO_@Ma@GY_@uB]}Bc@_CCSg@mCiAeH[mBQkAG[g@gCg@gCWwAQkAQeAGWK_@]q@Q]IIIQISIUIOGW@Yc@}AG_@KDKFIDEB]PWLc@\\MHo@b@SL{ClBa@ZM{@iAkGGYa@gC]qBS_AIDMDm@eD_@{BCKEKGCE?M@SJWNk@J"
                    },
                    "html_instructions": "Bus towards Leidseplein Amsterdam",
                    "travel_mode": "TRANSIT"
                  }
                ],
                "arrival_time": {
                  "text": "1:57pm",
                  "value": 1468497420,
                  "time_zone": "Europe/Amsterdam"
                },
                "end_address": "Amsterdam, Rijksmuseum, 1071 Amsterdam, Netherlands",
                "traffic_speed_entry": []
              }
            ],
            "warnings": [
              "Walking directions are in beta.    Use caution – This route may be missing sidewalks or pedestrian paths."
            ],
            "overview_polyline": {
              "points": "ejg~Hkqa\\?xFd@]Ea@IPID_@n@[v@@FOLOf@IRIjA_ClCm@b@kA~AGFCNG`@?`D@pBI|@G\\M`@GZEl@Cv@FjANr@jBnGt@|CNjAFfAJxEGhACZSv@aAzCq@fAs@v@_@Vi@Pe@Hi@B{@MgAe@cBw@wAa@qBe@qCk@]MoAg@kAo@eBeA_By@qDcCmA{@gA_AsAmA}CsDa@c@_@k@aA_Bs@gAm@eAWw@OcAEqAAsAFaC\\kCd@mDLwABcAAkBQoMi@md@I{ECWk@cB{@yB_D{DmCyCw@P{@gAUKEsAoBsBoB_Cw@aA]m@gAiCc@}AYaCIcACiAEeB?oEAyF?_CCwACWJ_APmABGn@mB|@uBLBf@gBxC{GnBqEp@eBB]DiBAiAIoHGwAWqAaAkBAKYg@GLqAoBi@_AJUO[mAgBe@{@vGiOJKb@_AHQD[A_@EMQW_@m@m@y@i@_AOSi@y@Si@cAmAcCaE}AwC{@gBMWOGU?OPiArBaBrCiBnCc@Ze@L[B[Ew@_@a@a@sBmCy@qA]k@_AyByIuOcB_Du@wAmAcBgF_Ko@eBKYKKYMMAAw@Ee@_AyCoFuNe@uAGa@?MSc@K[k@}AWi@UIYECVMv@IRUVuAhAUNEJMLG@YNMAKOIIEQ_@qCOcBk@aHOoCMyKQeA?mE@Q?wA?wABmE?}ABuI@sB?yBBaF?qB@gC?aEBaS?u@@_ABuB?eABq@BkBJoBVeGf@yFl@cGr@oHT{DF}AD{B@}FGsEMsFKkEKmEEiB?Y?BGiDGkESaIIsA?mAEiDOaGj@EEiDAcBo@Aq@UIRi@A{@Dg@?c@BWYKQCGu@?q@?}@@sCOo@Ko@Ig@Ca@BuBl@o@LiARaCj@QHu@AERaAZ{Ah@S@QBQGSc@Iq@KqBCWSGU?YD_AFgB?uBFoBBq@?eCJy@@gAFQ?KLGMO?a@EEUIOQMMESBSL[y@UkAiAuGg@qD]}AkAgHe@wC}@cFiAoGyAmJ_AiH[wASo@U{@}@sFg@sCqBsLm@yDoBcKc@qCSw@o@oAS[e@qA@Yc@}AG_@KDULc@T{@j@}@l@oDzBa@ZM{@qAeH_AyFS_AIDMDm@eDc@gCEKGCS@k@Zk@J"
            }
          }
        ],
        "geocoded_waypoints": [
          {
            "place_id": "ChIJHU4RWyjhxUcR27TNSXntfQs",
            "geocoder_status": "OK",
            "types": [
              "locality",
              "political"
            ]
          },
          {
            "place_id": "ChIJ10sv_e4JxkcRZpPLFMEYtHM",
            "geocoder_status": "OK",
            "partial_match": true,
            "types": [
              "bus_station",
              "establishment",
              "light_rail_station",
              "point_of_interest",
              "transit_station"
            ]
          }
        ]
      }
    }
  }
};