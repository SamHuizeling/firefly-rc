import {HttpService} from '../../services/http.service';
import {transitModes} from '../../utils/journey.utils';
import {tickets, journey} from './tickets.mock';
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {timer} from 'rxjs/observable/timer';
import {LocationService} from '../../services/location.service';

@Injectable()
export class TicketsService {
  ticketInfo: any = null;
  ticket: any;
  history: any;
  plan: any;
  transitModesList: string[];
  pushingGPS: boolean = false;
  ticketId : string;


  constructor(private httpService: HttpService,
    private locationService: LocationService) { }

  getHistory() {
    console.log('getHistory called');
    return this.httpService.get("/journey/history.json")
      .map(res => {
        console.log("history received", res.json());
        this.history = res.json();
        if (this.history.length > 0) {
          this.ticket = this.history[0];
          console.log(this.ticket);
          return true;
        }
        return false;
      });
  }

  makeTicketInfo() {
    let dict = this.plan.transit.total_fare_plan;
    let list = [];
    list.push("date: " + dict['departure_time'].slice(0, 10));
    list.push("time: " + dict['departure_time'].slice(11, 16));
    list.push("from: " + dict['departure_stop']);
    list.push("to: " + dict['arrival_stop']);
    this.ticketInfo = list;
  }

  getTicket(ticketId) {
    return this.httpService.get("/ticket/" + ticketId + "/?format=json")
      .do(ticket => this.ticket = ticket.json())
  }

  getTransitModes(i: number) {
    this.httpService.get_ext(this.history[i].journey_request.plan_file)
      .map(plan => plan.json())
      .subscribe((plan) => this.history[i].transitModes = transitModes(plan))
  }

  getHistoryPlan() {
    let i = 0;
    for (var journey of this.history) {
      this.getTransitModes(i);
      i++;
    }
  }

  getPlan() {
    return this.httpService.get_ext(this.ticket.journey_request.plan_file)
    // use this for local development:
    // return this.httpService.get_ext('http://192.168.178.41:8000' + this.ticket.journey_request.plan_file)
      .map(res => {
        this.plan = res.json();
        console.log("plan_file received", this.plan);
        this.makeTicketInfo();
      });
  }

  postGps(pos){
      return this.httpService.post("/journey/" + this.ticketId + "/check_status/", JSON.stringify(pos))
      .map(res => res.json())
      .do(res => console.log('completed response:', res));    
  }
      

  checkStatus() {
    // post the current lat lon to the /checkStatus
    // endpoint and retrieve the journey status
    return this.locationService.geoLocation
      .map(pos => {
        let myPos = { lat: pos.coords.latitude, lon: pos.coords.longitude };
        return myPos;
      })
      .do(pos => console.log("checkstatus pos: ", pos))
      .flatMap(pos=>this.postGps(pos));
  }

ticketStatus(){  
  return this.checkStatus()
  .takeWhile(res => { return !res.completed; })
}



  // _pushGPS() {
  //     this.checkStatus(this.ticketId)
  //       .do(res => {
  //         this.completedMessage = res.message;
  //         if (res.completed) {
  //           if (res.message === "expired") {
  //             this.isCompleted = true;
  //           }
  //           if (res.message === 'completed') {
  //             this.completedMessage = 'active';
  //             let toast = this.toastController.create({
  //               message: 'Your journey is completed.',
  //               duration: 3000,
  //               position: 'bottom'
  //             });
  //             toast.present();
  //           }
  //         } else {
  //           this.completedMessage = 'active';
  //         }
  //       })
  //       .takeWhile(res => { return !res.completed; })
  //       .subscribe(
  //       res => {
  //         console.log(res);
  //         let message = res['message'];
  //         if (_.isNumber(message)) {
  //           this.distance = message.toFixed(2);
  //         } else {
  //           this.distance = null;
  //         }

  //       }
  //       );
  //   }
  // }
}
