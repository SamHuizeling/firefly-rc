import {Itinerary, ItineraryComponent, extractItinerary} from '../../components/itinerary/itinerary';
import {MenuController, AlertController, ToastController, NavController, NavParams} from 'ionic-angular';
import {MapService} from '../../components/map/map.service';
import {HttpService} from '../../services/http.service';
import {MapComponent} from '../../components/map/map';
import {TicketsService} from './tickets.service';
import {doAlert} from '../../utils/interaction';
import {DateTimePipe} from './tickets.pipes';
import {timer} from "rxjs/observable/timer";
import {Component} from '@angular/core';
import {Endpoint} from '../../constants';
import {History} from '../history/history';
import {Subscription} from 'rxjs/Subscription';
import {LocationService} from '../../services/location.service';
import * as _ from 'lodash';

@Component({
  templateUrl: 'tickets.html',
})

export class Tickets {
  public mapName: string = "ticket";
  // journey_id: string;
  time: Object;
  itinerary: Itinerary[];
  completedMessage: string = null;
  distance: number = null;
  ticketId: string = null;
  qrcode_url: string = null;
  isCompleted: boolean = false;
  completedSubscribtion: Subscription;
  expiredSubscribtion: Subscription;

  constructor(private alertController: AlertController,
    private toastController: ToastController,
    private ticketsService: TicketsService,
    private mapService: MapService,
    private menu: MenuController,
    private navController: NavController,
    private navParams: NavParams,
    public httpService: HttpService,
    public locationService: LocationService
  ) {
    this.ticketId = navParams.get('ticketId');
    console.log('got ticket id:', this.ticketId);
    this.menu.swipeEnable(false);
    this.mapName = 'ticket';
    // this.time = {
    //   hours: '',
    //   minutes: '',
    //   seconds: '',
    //   milliseconds: ''
    // };
  }

  onBack() {
    console.log('back button clicked');
  }

  // Ik heb hier een klok gemaakt, super awesome

  ionViewWillLeave() {
    console.log("Looks like I'm about to leave :(");
    this.completedSubscribtion.unsubscribe();
    this.expiredSubscribtion.unsubscribe();
  }

  onTicketResponse(res) {
    console.log('got res:', res);
    this.completedMessage = 'active';
  }

  checkExpired() {
    this.expiredSubscribtion= timer(0,60000)
      .flatMap(() => this.ticketsService.postGps(this.locationService.lastLocation))
      .takeWhile(res => res.message != 'expired')
      .subscribe((res) => this.onTicketResponse(res), (err) => { }, () => this.onTicketExpiration())
  }

  onTicketCompletion() {
    this.completedMessage = 'active';
    let toast = this.toastController.create({
      message: 'Your journey is completed.',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
    this.checkExpired();
  }

  onTicketExpiration() {
    this.completedMessage = 'expired';
    let toast = this.toastController.create({
      message: 'Your ticket is expired.',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
    this.isCompleted = true;
  }


  showTicketInfo() {
    this.completedMessage = 'active';
    this.completedSubscribtion = this.ticketsService.getPlan() // todo: we dont have to do this if ticket has just been bought! (optimization)
      .delay(700) // wait for map resize
      .do(() => {
        this.itinerary = extractItinerary(this.ticketsService.plan);
        this.mapService.addPolyline({ plan: this.ticketsService.plan });
        console.log('itinerary', this.itinerary);
      })
      .flatMap(() => this.ticketsService.ticketStatus())
      .subscribe((res) => this.onTicketResponse(res), (err) => { }, () => this.onTicketCompletion())
  }

  ngOnInit() {
    console.log("OnInit ticket", this.mapName);
    if (this.httpService.is_authenticated) {
      if (!this.ticketId) {
        this.ticketsService.getHistory()
          .subscribe((ticketsfound) => {
            if (ticketsfound) {
              console.log('ticketsFound:', ticketsfound);
              this.showTicketInfo();
              this.ticketId = this.ticketsService.ticket.id.toString();
              this.ticketsService.ticketId = this.ticketsService.ticket.id.toString();
              this.qrcode_url = Endpoint.url + "/validate/" + this.ticketId + "/qrcode"
              console.log(this.qrcode_url);
              console.log('id = ', this.ticketId);
            } else {
              doAlert(this.alertController, "You have no tickets yet", "Plan your first ticket!");
              this.navController.pop();
            }
          });
      } else {
        this.ticketsService.getHistory().subscribe() // refresh the ticket history
        console.log('ticketsService.ticket=', this.ticketsService.ticket);
        this.qrcode_url = Endpoint.url + "/validate/" + this.ticketId + "/qrcode"
        this.ticketsService.ticketId = this.ticketId;
        this.ticketsService.getTicket(this.ticketId)
          .subscribe(() => this.showTicketInfo())
      }
    } else {
      doAlert(this.alertController, "You're not logged in.", "Login first.");
      this.navController.pop();
    }


    timer(500).subscribe(() => this.mapService.resizeEvent());
    // timer(100, 100).subscribe(() => {
    //   this.time = {
    //     hours: new Date().getHours(),
    //     minutes: new Date().getMinutes(),
    //     seconds: new Date().getSeconds(),
    //     milliseconds: new Date().getMilliseconds().toString()[0]
    //   };
    // });
  }

  presentModal() {
    this.navController.push(History);
  }

}



//   ngOnInit() {
//     console.log("OnInit ticket", this.mapName);

//     if (this.ticketsService.ticket) { // user just bought a new ticket, so we have it already:
//       console.log('ticketsService.ticket=', this.ticketsService.ticket);
//       this.showTicketInfo();
//       this.journey_id = this.ticketsService.ticket.journey.id.toString();
//     } else {
//       this.ticketsService.getHistory()
//         .subscribe((ticketsfound) => {
//           if (ticketsfound) {
//             console.log('ticketsFound:', ticketsfound);
//             this.showTicketInfo();
//             this.journey_id = this.ticketsService.ticket.journey.id.toString();
//             console.log('id = ', this.journey_id);
//           } else {
//             doAlert(this.alertController, "You have no tickets yet", "Press <- to go back and plan your first ticket!");
//           }
//         });
//     }
//     timer(500).subscribe(() => this.mapService.resizeEvent());
//     timer(100, 100).subscribe(() => {
//       this.time = {
//         hours: new Date().getHours(),
//         minutes: new Date().getMinutes(),
//         seconds: new Date().getSeconds(),
//         milliseconds: new Date().getMilliseconds().toString()[0]
//       };
//     });
//   }
// }
