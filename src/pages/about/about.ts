import {NavController, NavParams} from 'ionic-angular';
import {Component, OnInit} from '@angular/core';

@Component({
  templateUrl: 'about.html'
})

export class About implements OnInit {
  icons: Boolean;
  subpages: any;
  page: any;

  constructor(private navController: NavController,
              private navParams: NavParams) {
    this.navParams = navParams;
    this.navController = navController;
    this.icons = false;
  }

  ngOnInit() {

  }

  openTerms() {
    this.navController.push(Terms);
  }

  openPrivacy() {
    this.navController.push(Privacy);
  }

/*  openSubPage(page) {
    console.log('OPENING', page);
    this.navController.push(SubPage, {page: page});
  }*/
}

@Component({
  templateUrl: '/privacy/privacy.html'
})

export class Privacy {}

@Component({
  templateUrl: '/terms/terms.html'
})

export class Terms {}