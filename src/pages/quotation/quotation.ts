import {AlertController, NavController, NavParams, LoadingController} from 'ionic-angular';
// import {MapComponent, MapService} from '../../components/map/index';
import {DatabaseService} from "../../services/database.service";
import {PlannerService, RoundTime} from '../planner/index';
import {Tickets, TicketsService} from '../tickets/index';
import {HttpService} from '../../services/http.service';
import {transitModes} from '../../utils/journey.utils';
import {handleError} from '../../utils/interaction';
import {Component, OnInit} from '@angular/core';
import {timer} from "rxjs/observable/timer";
import {Observable} from "rxjs";

@Component({
  templateUrl: 'quotation.html',
})

export class Quotation implements OnInit {
  transitModesList: string[];
  passengers: any;
  total_fare: any;
  plan: any;
  fare: any;
  skipConfirmation: boolean = false;

  constructor(private loadingController: LoadingController,
    private alertController: AlertController,
    private plannerService: PlannerService,
    private ticketsService: TicketsService,
    private navController: NavController,
    private httpService: HttpService,
    private navParams: NavParams,
    private databaseService: DatabaseService) {

    // INIT Services
    this.alertController = alertController;
    this.navController = navController;
    this.httpService = httpService;
    this.navParams = navParams;

    // INIT Variables
    // this.mapName = 'quotation';
    this.passengers = 1;
  }

  ngOnInit() {
    this.plan = this.navParams.data.plan;
    this.fare = parseFloat(this.plan.plan.transit.total_fare_plan.fare).toFixed(2);
    this.total_fare = this.fare;

    this.transitModesList = transitModes(this.plan.plan);

    // timer(500).subscribe(() => {
    //   this.mapService.addPolyline(this.navParams.data.plan);
    // });
    // timer(200).subscribe(() => this.mapService.resizeEvent());
    this.databaseService.get('skipConfirmation').subscribe(res => {
      if (res === undefined) {
        this.skipConfirmation = false;
      } else {
        this.skipConfirmation = res;
      }
      console.log("skipConfirmation:", this.skipConfirmation);
    }, (err) => {
      console.log(err);
      this.skipConfirmation = false;
    });
  }


  addPassenger() {
    this.passengers++;
    this.total_fare = (this.fare * this.passengers).toFixed(2);
    // this.calcUberPrice();
  }

  rmPassenger() {
    this.passengers > 1 ? this.passengers-- : 1;
    this.total_fare = (this.fare * this.passengers).toFixed(2);
    // this.calcUberPrice();
  }

  // onBuyTicket(){
  //   this.confirmation();

  // }

  confirmation() {
    console.log('navParams.data', this.navParams.data);
    let info = this.navParams.data['plan']['plan']['transit']['total_fare_plan'];
    console.log('info', info);
    console.log("this.skipConfirmation", this.skipConfirmation);

    if (!this.skipConfirmation) {
      let confirm = this.alertController.create({
        title: 'Confirmation',
        message: info.agency + ' line ' +
        info.line + ", from " +
        info.departure_stop + " to " +
        info.arrival_stop + ". " +
        info.departure_time.slice(0, 10) +
        " at " + info['departure_time'].slice(11, 16) + ". Price: "
        + (this.passengers * info.fare).toFixed(2) + "EUR",
        inputs: [
          {
            label: "Don't ask me again",
            type: "checkbox",
            value: "true",
            name: "dontshowagain",
            checked: false
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'I agree',
            handler: (skip) => {
              // data will be [] if not checked and ["true"] if checked:
              console.log("got it data", skip);

              if (skip.length > 0) {
                this.skipConfirmation = JSON.parse(skip);
                console.log("got it data json", skip);
              } else {
                this.skipConfirmation = false;

              }
              this.databaseService.set('skipConfirmation', this.skipConfirmation);
              confirm.dismiss().then(() => this.checkIn());
            }
          }
        ]
      });
      confirm.present();
    } else {
      console.log("conformation disabled per user request");
      this.checkIn();
    }
  }

  checkIn() {
    let spinner = this.loadingController.create({ content: "Confirming your ticket" });
    spinner.present();

    let payload = { id: this.navParams.data.plan.request.id, n_persons: this.passengers };
    this.httpService.post('/journey/confirm.json', payload)
      .subscribe(confirmation => {
        let data = confirmation.json();
        console.log('data:', data);
        spinner.dismiss();
        if (data['message']==="direct pay"){
          window.open(data['direct pay url'], '_system');
        } else if (data['message']['id'] ){
          console.log('ticket id found:',data['id'],'going to ticket directly');
          this.httpService.getProfile().subscribe() //update wallet
          // this.ticketsService.ticket=data.journey;
          this.navController.push(Tickets, {'ticketId':data['message']['id']});
        }
      },
        err => {
          console.log(err);
          handleError(this.alertController, err, "onSelectDestination", spinner);
        }
      );
    // .map((journey) => this.ticketsService.ticket = journey.json())
    // .flatMap(() => this.httpService.getProfile())
    // .flatMap(() => Observable.fromPromise(spinner.dismiss()))
    // .subscribe(
    //   journey => {
    //     this.navController.push(Tickets);
    //   },
    //   err => {
    //     console.log(err);
    //     handleError(this.alertController, err, 'checkInTransit', spinner); // for iphone
    //   });
    // Todo: copy logic from planner.ts to here to make sure the ticket is created after hit the "buy now" button
  }
}
