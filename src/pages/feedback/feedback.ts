import {Component} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {AlertController} from 'ionic-angular';
import {doAlert} from '../../utils/interaction';
import {Device} from 'ionic-native';
import {HttpService} from '../../services/http.service';

@Component({
  templateUrl: 'feedback.html',
})

export class Feedback {
  feedbackForm: FormGroup;

/*    constructor(private fb: FormBuilder,
        public httpService: HttpService) {
            this.feedbackForm = fb.group({
            "name": [this.httpService.profile.first_name + " " + this.httpService.profile.first_name],
            "email":[this.httpService.profile.email, Validators.required],
            "feedback": [""]
        });
    }*/

    constructor(private fb: FormBuilder,
        private alertController: AlertController,
        public httpService: HttpService) {
        if (this.httpService.is_authenticated){
            this.feedbackForm = fb.group({
            "name":[this.httpService.profile.first_name + " " + this.httpService.profile.last_name, Validators.minLength(2)],
            "email":[this.httpService.profile.email, Validators.required],
            "feedback": ["", Validators.required]
            });
        } else {
            this.feedbackForm = fb.group({
            "name":["", Validators.minLength(2)],
            "email":["", Validators.required],
            "feedback": ["", Validators.required]
            });
        }
    }

    onSubmit() {
        console.log(Device.device.version);
        if (this.feedbackForm.valid) {
          let feedbackPost = {
            user_name: this.feedbackForm.value.name,
            email: this.feedbackForm.value.email,
            feedback: this.feedbackForm.value.feedback,
            android_version: Device.device.platform + " " + Device.device.version,
            device_name: Device.device.manufacturer + " " + Device.device.model
            };
          this.httpService.post('/feedback/', feedbackPost)
            .do((res) => doAlert(this.alertController, 'Feedback posted!','Thank you for helping FireFly.'))
            .do((res) => (<FormControl>this.feedbackForm.controls['feedback']).setValue(''))
            .subscribe(
                (res) => console.log("Feedback posted!", res),
                (err) => doAlert(this.alertController, 'There seems to be something wrong.')
            );
        }
    }
}