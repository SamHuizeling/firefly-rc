import {AuthenticationComponent} from '../../components/authentication/authentication';
import {PaymentComponent} from '../../components/payment/payment';
import {Component} from '@angular/core';
import {DatabaseService} from '../../services/database.service';
import {HttpService} from '../../services/http.service';
import {MenuController} from 'ionic-angular';

@Component({
  templateUrl: 'account.html',
})

export class Account {
  constructor(public databaseService: DatabaseService,
    public httpService: HttpService,
    private menu: MenuController) {
    this.menu.swipeEnable(false);
  }

  logout() {
    this.databaseService.removeTokens();
    this.httpService.is_authenticated = false;
  }
}
