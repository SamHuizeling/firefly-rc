export function operator(plan) {
  if (plan.hasOwnProperty('transit')) {
    let len = plan.transit.response.routes[0].legs.length;
    let bounds = plan.transit.response.routes[0].bounds;
    let legs = plan.transit.response.routes[0].legs;
    for (var leg in legs) {
      // endLocation = leg.end_location;
      for (var step in legs[leg].steps) {
        if (legs[leg].steps[step].travel_mode === "TRANSIT") {
          let line = legs[leg].steps[step].transit_details.line;
          if (line.vehicle.type === "BUS") {
            let operator = line.agencies[0].name;
            return operator;
          }
        }
      }
    }
  }
  return null;
}



export function transitModes(plan) {

// different transit types:
// https://developers.google.com/maps/documentation/directions/intro
/*
RAIL	Rail.
METRO_RAIL	Light rail transit.
SUBWAY	Underground light rail.
TRAM	Above ground light rail.
MONORAIL	Monorail.
HEAVY_RAIL	Heavy rail.
COMMUTER_TRAIN	Commuter rail.
HIGH_SPEED_TRAIN	High speed train.
BUS	Bus.
INTERCITY_BUS	Intercity bus.
TROLLEYBUS	Trolleybus.
SHARE_TAXI	Share taxi is a kind of bus with the ability to drop off and pick up passengers anywhere on its route.
FERRY	Ferry.
CABLE_CAR	A vehicle that operates on a cable, usually on the ground. Aerial cable cars may be of the type GONDOLA_LIFT.
GONDOLA_LIFT	An aerial cable car.
FUNICULAR	A vehicle that is pulled up a steep incline by a cable. A Funicular typically consists of two cars, with each car acting as a counterweight for the other.
OTHER	All other vehicles will return this type.
*/

interface TransitModeToIconMap {
    [transit: string]: string;
}

var map: TransitModeToIconMap = { };
map['RAIL'] = 'train';
map['METRO_RAIL'] = 'subway';
map['SUBWAY'] = 'subway';
map['TRAM'] = 'subway';
map['MONORAIL'] = 'train';
map['HEAVY_RAIL'] = 'train';
map['COMMUTER_TRAIN'] = 'train';
map['HIGH_SPEED_TRAIN'] = 'train';
map['BUS'] = 'bus';
map['INTERCITY_BUS'] = 'bus';
map['TROLLEYBUS'] = 'bus';

map['FERRY'] = 'boat';
map['CABLE_CAR'] = 'SUBWAY';
map['GONDOLA_LIFT'] = 'minus';
map['FUNICULAR'] = 'minus';
map['OTHER'] = 'minus';

  let modes = [];
  if (plan.hasOwnProperty('transit')) {
    let len = plan.transit.response.routes[0].legs.length;
    let bounds = plan.transit.response.routes[0].bounds;
    let legs = plan.transit.response.routes[0].legs;
    for (var leg in legs) {
      // endLocation = leg.end_location;
      for (var step in legs[leg].steps) {
        if (legs[leg].steps[step].travel_mode === "WALKING") {
          if (Math.round(legs[leg].steps[step].duration.value / 60) !== 0) {
            modes.push({
              icon: 'walk',
              text: Math.round(legs[leg].steps[step].duration.value / 60) + ' min'
            });
          }
        }
        if (legs[leg].steps[step].travel_mode === "TRANSIT") {
          let line = legs[leg].steps[step].transit_details.line;
          let transit_mode = line.vehicle.type;

          modes.push({
            icon: map[transit_mode],
            text: line.short_name
          });
        }
      }
    }
  }
  return modes;
}

export function journeyInstructions(plan) {
  let instructions = "";
  if (plan.hasOwnProperty('transit')) {
    let len = plan.transit.response.routes[0].legs.length;
    let bounds = plan.transit.response.routes[0].bounds;
    let legs = plan.transit.response.routes[0].legs;
    for (var leg in legs) {
      // endLocation = leg.end_location;
      for (var step of legs[leg].steps) {
        instructions = instructions + step.html_instructions + ". ";

      }
    }
  }
  return instructions;
}

export function line(plan) {
  if (plan.hasOwnProperty('transit')) {
    let len = plan.transit.response.routes[0].legs.length;
    let bounds = plan.transit.response.routes[0].bounds;
    let legs = plan.transit.response.routes[0].legs;
    for (var leg in legs) {
      // endLocation = leg.end_location;
      for (var step in legs[leg].steps) {
        if (legs[leg].steps[step].travel_mode === "TRANSIT") {
          let line = legs[leg].steps[step].transit_details.line;
          if (line.vehicle.type === "BUS") {
            let line_name = line.short_name;
            return line_name;
          }
        }
      }
    }
  }
  return null;
}
