import {AlertController, Loading} from 'ionic-angular';
import {Observable} from 'rxjs/Observable';

export function handleError(alertController: AlertController, error: any, context: string, loading: Loading = null) {
    // In a real world app, we might send the error to remote logging infrastructure
    let errMsg = error.message || 'Server error';
    if (loading) {
        loading.dismiss();
    }

    let jason = JSON.parse(error._body);
    console.log(jason);
    let message = jason.message ? jason.message : jason.error_description ? jason.error_description : jason.password && jason.password[0] ? jason.password[0] : "unknown";
    // todo: check which one is nicer:
    console.log(context, ' exception: ', error);
    console.log(context, ' exception: ' + JSON.stringify(error, null, 2));
    doAlert(alertController, 'Something seems not right:', message );
    return Observable.throw(errMsg);
}

export function doAlert(alertController: AlertController, title: string = null, subTitle: string = null) {
    console.log('alert title', title);
    console.log('alert subTitle', subTitle);
    if (!subTitle) {
        subTitle = "";
    }    
    let alert = alertController.create({
        title: (title.length < 40) ? title : "Oops",
        subTitle: (subTitle.length < 100) ? subTitle : "server message too long to show here",
        buttons: ['OK']
    });
    alert.present();
}
