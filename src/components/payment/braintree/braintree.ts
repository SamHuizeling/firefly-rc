import {HttpService} from '../../../services/http.service';
import {FormBuilder, Validators} from '@angular/forms';
import {IONIC_DIRECTIVES,ToastController} from 'ionic-angular';
import {Component, OnInit} from '@angular/core';
import {Platform} from 'ionic-angular';
import {timer} from 'rxjs/observable/timer';

declare var braintree;

@Component({
  selector: 'braintree',
  templateUrl: 'braintree.html',
})

export class BraintreeComponent {
  profile: any;
  cardSelectVisible: boolean;
  clientToken: string;

  constructor(private platform: Platform,
              private httpService: HttpService,
              private toastController: ToastController) {

    this.platform = platform;
    this.profile = this.httpService.profile;

    if (this.profile.card == null) {
      this.cardSelectVisible = true;
    } else {
      // display new card if the card is not valid
      this.cardSelectVisible = !this.profile.card.is_valid;
    }
  }

  ngOnInit() {
      this.httpService.get('/pay_bt/token.json').subscribe(res => {
      // todo: add in clause when token is not achieved, this would not necessarily occur
      console.log('clientToken gotten from server');
      this.clientToken = res.json().token;
      console.log('token = ', this.clientToken )
      this.platform.ready().then(() =>  this.initializeBraintree()); 
    })
  }
  

  initializeBraintree() {
    

      // Note: to enable CVV:  add CVV rules in the fraude detection in your sandbox settings
      // see: https://articles.braintreepayments.com/guides/fraud-tools/avs-cvv#enabling-avs-and-cvv-rules

      // Note2: onPaymentMethodReceived does not fire after paypal is added, it does after CC is added.
      // as such after paypal is added it must explicitely press the button, but that is ok as after you
      // press add the box will disappear
      // https://github.com/braintree/braintree-web/issues/58#issuecomment-118852093

      var clientToken = this.clientToken;

      var that = this;

      if (this.cardSelectVisible) {
        braintree.setup(clientToken, "dropin", {
          container: "addcard-container",
          paypal: {
            singleUse: false
          },
          onPaymentMethodReceived: function(obj) {

            console.log('a submit on the (creditcard) is done');
            console.log(obj);

            // method gets fired after button is pressed.
            // note: upon adding paypal you do have to explicitely submit button
            var payload = {
              payment_method_nonce: obj.nonce,
              payment_method: obj.type
            };

            // payment details depends on the type of card

            let payment_method_type: string;
            if (obj.type === 'PayPalAccount') {
              var payment_method_desc = obj.details.email;
              payment_method_type = 'PayPal';
            } else {
              var payment_method_desc = obj.details.lastTwo;
              payment_method_type = obj.details.cardType;
            }
            that.braintreeCallBack(payload, payment_method_desc, payment_method_type);

          }
        });
      }
  };

  removeCard() {

    this.cardSelectVisible = true;
    this.httpService.delete('/pay_bt.json').subscribe(res => {
      // todo: add in clause when card could not be verified, this would not necessarily occur
      console.log('posted card to server and got result');
      console.log(res);
      this.profile.card = null;
      this.cardSelectVisible = true;
      timer(1000).subscribe(()=>{this.cardSelectVisible = true; this.initializeBraintree()})
    });

    
  }

  // note this submit is just to fool ionic framework, the payment_nonce processing must
  // occur in the onPaymentMethodReceived function
  addCard(event) {
  }

  braintreeCallBack(payload, payment_method_desc, payment_method_type) {
    // POST /pay_bt/: using javascript custom xmlhttprequest. this is due to iframe implementation

    // adjust the profile to match the card
    // @ Jelle: alternatively you can also wait for the post request, but that would take a bit longer.
    // Anyhow, both methods are fine. Check with design ;)
    this.profile.card = {
      payment_method_type: payment_method_type,
      payment_method_desc: payment_method_desc,
      is_valid: true
    };
    this.cardSelectVisible = false;
    
    console.log('card visible is adjusted, but the screen is not refreshed... card: ' + this.cardSelectVisible);
    // TODO: @jelle, timing issue, solve according to autocomplete. This does not refresh the set new card

    this.httpService.post('/pay_bt.json', payload)
    .map(res=>res.json())
    .do(res=>this.showInvalidCardToast(res.is_valid))
    // .do(res=> this.cardSelectVisible = !res.is_valid)
    .subscribe(newCustomer => {
      // todo: add in clause when card could not be verified, this would not necessarily occur
      console.log('posted card to server and got result');
      console.log(newCustomer);
      if (!newCustomer.is_valid){
       timer(1000).subscribe(()=>this.removeCard())
      }
      //   this.cardSelectVisible = true;
      //   console.log('creditcard doenst seem valid');
      //   // timer(200).subscribe(()=>{this.initializeBraintree();console.log('still not valid!')})

      // } else {
      //   this.cardSelectVisible = false;
      // }
      

    });
  }

  showInvalidCardToast(cardIsValid){
          if (!cardIsValid){
          let toast = this.toastController.create({
            message: "Sorry, creditcard seems not valid",
            duration: 2000,
            position: 'middle'
          });
          toast.present();
          }

  }

}
