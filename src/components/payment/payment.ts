import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AlertController, LoadingController} from 'ionic-angular';
import {handleError, doAlert} from '../../utils/interaction';
import {BraintreeComponent} from './braintree/braintree';
import {HttpService} from '../../services/http.service';
import {Observable} from 'rxjs/Observable';
import {Component} from '@angular/core';

@Component({
  selector: 'payment',
  templateUrl: 'payment.html',
})

export class PaymentComponent {
  currency: string;
  voucherCtrl = new FormControl();
  voucherForm = new FormGroup({
      voucher: this.voucherCtrl
  });
  constructor(private loadingController: LoadingController,
              private alertController: AlertController,
              private httpService: HttpService) {

    this.currency = '€';
  }

  onVoucher() {
    if (this.voucherForm.valid) {
      let voucher = this.voucherForm.value.voucher;
      let spinner = this.loadingController.create({content: "checking your voucher"});
      spinner.present();
      this.httpService.get("/voucher/" + voucher + ".json")
        .do((res) => doAlert(this.alertController, 'yes!', res['_body']))
        .flatMap(() => this.httpService.getProfile())
        .flatMap(() => Observable.fromPromise(spinner.dismiss()))
        .subscribe(
          (res) => console.log("voucher valid", res),
          (err) => {
            console.log("invalid voucher", err);
            handleError(this.alertController, err, 'voucher', spinner);
          });
    } else {
      doAlert(this.alertController, 'Please enter your voucher');
    }
  }

}
