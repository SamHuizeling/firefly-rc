import {Platform, ToastController} from 'ionic-angular';
import {Input, Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {Observable}    from 'rxjs/Observable';
import {MapService} from './map.service';

@Component({
  selector: 'map',
  templateUrl: 'map.html'
})

export class MapComponent {
  public map: any;
  private centering: boolean = false;
  centerLocationSubscription: Subscription;
  @Input() map_name = "firstMap";

  constructor(private toastController: ToastController,
              private mapService: MapService,
              private platform: Platform) {
    console.log("Constructing map with id", this.map_name);
  }

  ngOnInit() {
    console.log("OnInit map with id", this.map_name);
    this.mapService.dom_selector = this.map_name;
    this.map = this.mapService.map;
    this.mapService.initMap();
    this.mapService.drawMyLocation()
      .subscribe(() => {}, (err) => this.handleError(err));
  }

  handleError(error: any) {
    let errMsg = error.message || 'Gps error';

    // todo: check which one is nicer:
    console.log(' exception: ', error);
    console.log(' exception: ' + JSON.stringify(error, null, 2));
    let toast = this.toastController.create({
      message: errMsg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
    return Observable.throw(errMsg);

  }

  toastMyLocation(location) {
    // generates a toast with the location updates
    let toast = this.toastController.create({
      message: location.coords.latitude + "  " + location.coords.longitude,
      duration: 2000,
      position: 'top'
    });
    toast.present();

  }

  toastCentering(centering) {
    let msg: string;
    if (centering) {
      msg = "Centering map on your location";
    } else {
      msg = "Done Centering";
    }
    let toast = this.toastController.create({
      message: msg,
      duration: 1500,
      position: 'top'
    });
    toast.present();


  }

  onCenterLocation() {
    this.centering = !this.centering;
    if (this.centering) {
      this.centerLocationSubscription = this.mapService.centerMyLocation().subscribe();
    } else {
        this.centerLocationSubscription.unsubscribe();
    }
    this.toastCentering(this.centering);
    console.log('centering:', this.centering, 'subscription:', this.centerLocationSubscription);
  }
}
