import {LocationService} from '../../services/location.service';
import {Injectable} from '@angular/core';
import {Platform} from 'ionic-angular';
import {styles} from './map.styles';

declare var google;

@Injectable()
export class MapService {
  public dom_selector: string;
  currentLocation: any;
  myLocationMarker: any;
  startmarker: any;
  polylines: any[];
  endmarker: any;
  map: any;

  constructor(private locationService: LocationService,
              private platform: Platform) {

    this.platform = platform;
    this.currentLocation = {};
    this.polylines = [];
  }

  resizeEvent() {
    google.maps.event.trigger(this.map, 'resize');
  }

  drawMyLocation() {
    console.log('drawing location');
    return this.locationService.geoLocation
      .map((position) => {
        if (this.myLocationMarker) {
          this.myLocationMarker.setMap(null);
        }
        this.currentLocation = {lat: position.coords.latitude, lng: position.coords.longitude};
        // todo : save this file localy:
        var image = 'http://i.stack.imgur.com/orZ4x.png';
        this.myLocationMarker = new google.maps.Marker({
          position: this.currentLocation,
          map: this.map,
          icon: image
        });
        this.myLocationMarker.setMap(this.map);
        return position;
      });
  }

  centerOnce() {
    return this.centerMyLocation()
      .do(() => console.log('map centered'))
      .first();
  }

  centerMyLocation() {

    return this.locationService.geoLocation
      .do((pos) => console.log('centering map around location', pos))
      .map((position) => {
        this.currentLocation = {lat: position.coords.latitude, lng: position.coords.longitude};
        this.map.setCenter(this.currentLocation);
      });
  }

  initMap() {
    console.log('initializing map');
    this.platform.ready().then(() => {
      let options = {
        center: new google.maps.LatLng(52.089319, 5.110169),
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        styles: styles
      };

      this.map = new google.maps.Map(document.getElementById(this.dom_selector), options);
    });

  }

  removePolyLines() {
    if (this.polylines.length > 0) {
      for (var line of this.polylines) {
        line.setMap(null);
      }
      this.endmarker.setMap(null);
      this.polylines = [];
    }
  }

  addPolyline(journey) {
    if (journey.hasOwnProperty('plan')) {
      let startLocation = journey.plan.transit.response.routes[0].legs[0].start_location;
      // let endLocation: any;
      // journey.plan.transit.response.routes[journey.plan.transit.response.routes.length - 1].legs.end_location;
      let len = journey.plan.transit.response.routes[0].legs.length;
      let endLocation = journey.plan.transit.response.routes[0].legs[len - 1].end_location;
      var bounds = journey.plan.transit.response.routes[0].bounds;
      var legs = journey.plan.transit.response.routes[0].legs;
      for (var leg in legs) {
        // endLocation = leg.end_location;
        for (var step in legs[leg].steps) {
          let polyline = legs[leg].steps[step].polyline.points;
          let pathCoordinates = google.maps.geometry.encoding.decodePath(polyline);
          let lineSymbol: any;
          // let color = legs[leg].steps[step].travel_mode == 'TRANSIT' ? 'rgba(39, 169, 225, 1)' : 'rgba(241, 101, 33, 1)';
          let color;
          let path: any;
          // Todo: empty polyline new request
          // console.log('travel mode:' + legs[leg].steps[step].travel_mode);
          if (legs[leg].steps[step].travel_mode === 'WALKING') {
            color = 'rgba(50, 122, 196, 1)';
            lineSymbol = {
              path: google.maps.SymbolPath.CIRCLE,
              strokeColor: color,
              strokeOpacity: 0.9,
              fillOpacity: 0.9,
              scale: 3
            };
            path = new google.maps.Polyline({
              path: pathCoordinates,
              strokeOpacity: 0,
              icons: [{icon: lineSymbol, offset: '0', repeat: '10px'}]
            });
          } else {
            color = 'rgba(50, 122, 196, 1)';
            path = new google.maps.Polyline({
              path: pathCoordinates,
              geodesic: true,
              strokeColor: color,
              strokeOpacity: 0.9,
              strokeWeight: 4
            });
          }
          this.polylines.push(path);
          this.polylines.slice(-1)[0].setMap(this.map);
        }
      }

      var mapBounds = new google.maps.LatLngBounds();
      for (var bound in bounds) {
        mapBounds.extend(new google.maps.LatLng(bounds[bound].lat, bounds[bound].lng));
      }
      this.map.fitBounds(mapBounds);
      this.map.fitBounds(mapBounds);
      // this.startmarker = new google.maps.Marker({ position: startLocation, title: 'start', color: 'rgba(0,255,0,1)' });
      // this.startmarker.setMap(this.map);
      this.endmarker = new google.maps.Marker({position: endLocation, title: 'start'});
      this.endmarker.setMap(this.map);
    }
  }
}


