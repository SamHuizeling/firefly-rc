import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertController, LoadingController, NavController} from 'ionic-angular';
import {handleError, doAlert} from '../../utils/interaction';
import {HttpService} from '../../services/http.service';
import {Component} from '@angular/core';
import {Terms, Privacy} from '../../pages/about/about'

@Component({
  selector: 'authentication',
  templateUrl: 'authentication.html',
})

export class AuthenticationComponent {
  authenticationMode: string;
  socialSignIn: boolean;
  signupForm: FormGroup;
  loginForm: FormGroup;
  profile: any;
  page: any;

  constructor(private loadingController: LoadingController,
    private alertController: AlertController,
    private formBuilder: FormBuilder,
    private navController: NavController,
    public httpService: HttpService) {


    // FORMS
    this.loginForm = this.formBuilder.group({
      email: ["", Validators.required],
      password: ["", Validators.required]
    });



    this.signupForm = this.formBuilder.group({
      firstName: [""],
      lastName: [""],
      email: ["", Validators.required],
      password: ["", Validators.required],
      cb: [false, Validators.required] // https://github.com/angular/angular/issues/11459 Validators.required zit nog niet in angular rc6 zie link. Dirty solution in HTML
    });

    this.authenticationMode = 'login';
    this.socialSignIn = true;
  }

  onSignUp() {
    if (this.signupForm.valid) {
      // todo logger(functionContext: this.signupForm.value.firstName)
      let payload = {
        first_name: this.signupForm.value.firstName,
        last_name: this.signupForm.value.lastName,
        email: this.signupForm.value.email,
        password: this.signupForm.value.password
      };
      let spinner = this.loadingController.create({
        content: "Setting up your account",
        dismissOnPageChange: true
      });
      spinner.present();

      this.httpService.post('/auth/signup/', payload)
        .flatMap(() => this.login(payload.email, payload.password))
        .do(() => spinner.dismiss())
        .subscribe(
        (res) => console.log("signed up!", res),
        (err) => {
          console.log("signing up failed", err);
          handleError(this.alertController, err, 'signup', spinner);
        });
    } else {
      doAlert(this.alertController, 'Please enter your name, email address and password');
    }
  }

  onLogin(event) {
    if (this.loginForm.valid) {
      let spinner = this.loadingController.create({ content: "Logging in" });
      spinner.present();
      this.login(this.loginForm.value.email, this.loginForm.value.password)
        .do(() => spinner.dismiss())
        .subscribe(
          (res) => console.log("logged in", res),
          (err) => {
            console.log("logging in failed", err);
            handleError(this.alertController, err, 'login', spinner);
          });
    } else {
      doAlert(this.alertController, 'Please enter your email address and password');
    }
  }

  login(email, password) {
    return this.httpService.update_or_create_tokens(email, password)
      .flatMap(res => this.httpService.getProfile());
  }

  socialSigninAlert() {
    doAlert(
      this.alertController,
      'Tricked you with mock buttons?!',
      'Facebook/Twitter will be available in one of the next versions. We just want to show you the awesome ideas we have in mind...'
    );
  }

  openTerms() {
    this.navController.push(Terms);
  }

  openPrivacy() {
    this.navController.push(Privacy);
  }
}
