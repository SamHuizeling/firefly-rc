export let MockData = {
  "transit": {
    "total_fare_plan": {
      "arrival_time": "2016-08-29 12:07:00",
      "arrival_stop": "Amsterdam, Rijksmuseum",
      "line_name": "Schiphol P30 - Amsterdam Leidseplein",
      "departure_time": "2016-08-29 11:31:00",
      "fare": 2.4817,
      "agency": "Connexxion",
      "departure_stop": "Schiphol, Skyport",
      "line": "197",
      "checkin_possible": true
    },
    "status_code": 200,
    "response": {
      "status": "OK",
      "routes": [
        {
          "copyrights": "Map data \u00a92016 Google",
          "bounds": {
            "northeast": {
              "lng": 4.884835000000001,
              "lat": 52.360154
            },
            "southwest": {
              "lng": 4.75075,
              "lat": 52.3075245
            }
          },
          "summary": "",
          "warnings": [
            "Walking directions are in beta.    Use caution \u2013 This route may be missing sidewalks or pedestrian paths."
          ],
          "legs": [
            {
              "arrival_time": {
                "text": "12:09pm",
                "time_zone": "Europe/Amsterdam",
                "value": 1472465356
              },
              "start_location": {
                "lng": 4.7645433,
                "lat": 52.307707
              },
              "departure_time": {
                "text": "11:29am",
                "time_zone": "Europe/Amsterdam",
                "value": 1472462984
              },
              "steps": [
                {
                  "polyline": {
                    "points": "ejg~Hkqa\\@jDAlAVULGEa@"
                  },
                  "start_location": {
                    "lng": 4.7645433,
                    "lat": 52.307707
                  },
                  "fare_plan": null,
                  "duration": {
                    "text": "1 min",
                    "value": 76
                  },
                  "end_location": {
                    "lng": 4.76361,
                    "lat": 52.307553
                  },
                  "distance": {
                    "text": "0.1 km",
                    "value": 108
                  },
                  "steps": [
                    {
                      "polyline": {
                        "points": "ejg~Hkqa\\@jDAlA"
                      },
                      "start_location": {
                        "lng": 4.7645433,
                        "lat": 52.307707
                      },
                      "duration": {
                        "text": "1 min",
                        "value": 61
                      },
                      "end_location": {
                        "lng": 4.7632939,
                        "lat": 52.30770589999999
                      },
                      "distance": {
                        "text": "85 m",
                        "value": 85
                      },
                      "html_instructions": "Head <b>west</b> on <b>Inspecteurspassage</b> toward <b>Havenmeesterweg</b>",
                      "travel_mode": "WALKING"
                    },
                    {
                      "polyline": {
                        "points": "ejg~Hqia\\VULGEa@"
                      },
                      "start_location": {
                        "lng": 4.7632939,
                        "lat": 52.30770589999999
                      },
                      "duration": {
                        "text": "1 min",
                        "value": 15
                      },
                      "maneuver": "turn-left",
                      "end_location": {
                        "lng": 4.76361,
                        "lat": 52.307553
                      },
                      "distance": {
                        "text": "23 m",
                        "value": 23
                      },
                      "html_instructions": "Turn <b>left</b> onto <b>Havenmeesterweg</b><div style=\"font-size:0.9em\">Destination will be on the left</div>",
                      "travel_mode": "WALKING"
                    }
                  ],
                  "html_instructions": "Walk to Schiphol, Skyport",
                  "travel_mode": "WALKING"
                },
                {
                  "polyline": {
                    "points": "eig~Hqka\\IPIDIJUb@Qf@IN?@@DOLOf@IRIjA?A_CnCm@b@kA~AGFCNELAR?fC?X@j@?dAEd@CVAJEPEPGNCNCJC^ALAVA^Bd@Bd@F\\FTJZ~ArFf@pBLj@H`@Dh@B^?P?@BRDpADfCCf@C`@CZSv@e@|A[|@Yf@W^W\\[X_@Vi@Pe@Hi@B_@E[G]Mi@W_@ScAc@}@YYGwA[YIqCk@]MoAg@kAo@k@]y@g@_By@qDcCmA{@gA_Ay@u@YWyBiCc@i@a@c@_@k@g@y@Ye@OWc@o@KUa@o@KWK_@Ia@Ea@Ca@Ao@Ae@?m@BcAB}@L}@NmAT}ANoALwABcA@q@Cy@IaGGmEMyJMyLGgFEqCGsDAg@?ICMGSc@oAa@eAYs@o@{@oB_CmCyCw@P{@gAUKEsAoBsBqA_B]_@a@e@U[KQQ[i@iA]_Aa@yAACGs@QmAIcACiA?q@Es@?iB?eBC}C@{A?}@?aACo@?g@CWJ_ADc@Ji@@G@?Po@\\}@Xm@b@gALBf@gBxB_F^{@f@kAfAeCp@eBB]DiBAiACiCEeDC_ACWWqAaAkB?EAEYg@GLi@u@g@y@KS]k@JUO[mAgBe@{@vGiOJKZq@FMDKBEBO@K@ICUAECGIKGKQYMSGEe@s@[e@MYAAMQi@y@Si@w@{@KQQYqBgDmA}BOYSa@IQQ_@KSEMGIEEIAGAM@EDIJMV{@zAg@v@y@zA_@j@]h@[f@OPKJWNQFSDMBM?OAKCYK]SQSOMi@q@AAe@q@a@g@U_@c@q@]k@_AyBaGmKwAgCaBwCAGi@eAKQKMaAuAeBgDkA}Ba@y@S_@Ws@Wq@KYKKKGMEMAAw@ASCQ]eAa@sAsDuJ{@_C_@eAEOEOAQ?MSc@KY?AYu@Qg@Qc@EEECOEYECVCXI\\IRUVYV{@p@UNEJMLG@QJGBGAE?CEGIIIEQ]gCAIGk@Gw@MoAKyAEk@KkAE_AIoAE}AG{HQeA?mE?K@E?Q?Y?S?W?Q?[?E?c@?c@@O?]?E?K?E@s@?o@?E?I?E?Q?K?E?K?W?O?Q@wA@eA?c@?Q?eA?Y@[?a@?k@?I?O@Q?QAeA?Q@s@?o@?E?]?U?Q@_@?S?Q?Q?QAQ?E@K?C?K?C?W?G?Q?Q?a@@Q?c@?O?Q?Q?s@?Q?Q?Q@iB?O?c@?_H@kB?_A?Q?E?K?E?]?E?I@E?]?Q@Q?O@o@?c@Aa@@c@@W@YBy@?q@Bc@FkADsADo@DeAD{@Fm@@OLyANaBDg@Fe@PiBLkAHu@PkBLsAHy@Dm@B]Bq@F}@Bs@Bi@Bs@@gA?_@@q@?mA?}AAk@?e@C}@AcAEcBAg@Ae@CaA?MAQ?AASAc@Ak@AYAk@AS?O?KI}CCeAAc@?Y?BEsCAUG_D?k@?AA_@Cq@MmFIsA?mACsBAu@KuECk@j@EEiDAcBo@Aq@UIRi@A{@D_@?G?W?KBMQIGGKCECGu@?G?i@?Q?k@@g@CkBKWCWG]EQCg@Ca@BQDcBf@K@c@Jc@He@HkAXu@PQHu@AERaAZgA^SHI@I?E?KBEAKECEEICEEMEWCYCa@GoAAKAKICICQ?C?MBK@K?s@FgB?uBF{@?s@Bq@?eCJ]?[@U@q@DQ?KLGMO?GAYCAGCMCCEKIGGEC?IEI?IBIDIFSe@GSI[Ko@i@cD_@qBYoBMaAGWUeAWcBWwAO{@Ko@Ga@G_@Ko@Ie@Q_AWwAEYMq@Kc@}@kFcAsGUyAYoBe@yDO{@K[COO_@Ma@GY_@uB]}Bc@_CCSg@mCiAeH[mBQkAG[g@gCg@gCWwAQkAQeAGWK_@]q@Q]IIIQISIUIOGW@Yc@}AG_@KDKFIDEB]PWLc@\\MHo@b@SL{ClBa@ZM{@iAkGGYa@gC]qBS_AIDMDm@eD_@{BCKEKGCE?M@SJWNk@J"
                  },
                  "start_location": {
                    "lng": 4.76361,
                    "lat": 52.307553
                  },
                  "fare_plan": {
                    "arrival_time": "2016-08-29 12:07:00",
                    "arrival_stop": "Amsterdam, Rijksmuseum",
                    "line_name": "Schiphol P30 - Amsterdam Leidseplein",
                    "departure_time": "2016-08-29 11:31:00",
                    "fare": 2.4817,
                    "agency": "Connexxion",
                    "departure_stop": "Schiphol, Skyport",
                    "line": "197",
                    "checkin_possible": true
                  },
                  "duration": {
                    "text": "36 mins",
                    "value": 2160
                  },
                  "end_location": {
                    "lng": 4.883353,
                    "lat": 52.360154
                  },
                  "distance": {
                    "text": "14.8 km",
                    "value": 14817
                  },
                  "transit_details": {
                    "arrival_time": {
                      "text": "12:07pm",
                      "time_zone": "Europe/Amsterdam",
                      "value": 1472465220
                    },
                    "arrival_stop": {
                      "name": "Amsterdam, Rijksmuseum",
                      "location": {
                        "lng": 4.883353,
                        "lat": 52.360154
                      }
                    },
                    "departure_time": {
                      "text": "11:31am",
                      "time_zone": "Europe/Amsterdam",
                      "value": 1472463060
                    },
                    "num_stops": 13,
                    "departure_stop": {
                      "name": "Schiphol, Skyport",
                      "location": {
                        "lng": 4.76361,
                        "lat": 52.307553
                      }
                    },
                    "line": {
                      "url": "http://www.connexxion.nl/dienstregeling/lijn?ID=M197",
                      "agencies": [
                        {
                          "phone": "011 31 900 2666399",
                          "url": "http://www.connexxion.nl/",
                          "name": "Connexxion"
                        }
                      ],
                      "name": "Schiphol P30 - Amsterdam Leidseplein",
                      "vehicle": {
                        "icon": "//maps.gstatic.com/mapfiles/transit/iw2/6/bus2.png",
                        "name": "Bus",
                        "type": "BUS"
                      },
                      "short_name": "197"
                    },
                    "headsign": "Leidseplein Amsterdam"
                  },
                  "html_instructions": "Bus towards Leidseplein Amsterdam",
                  "travel_mode": "TRANSIT"
                },
                {
                  "polyline": {
                    "points": "}qq~H}wx\\D\\@A~@m@RI@ABAB?@?B@B?@O@EBINa@DK@A?A@?BCVOBCDEBC@E@A@C{A{BKO"
                  },
                  "start_location": {
                    "lng": 4.883353,
                    "lat": 52.360154
                  },
                  "fare_plan": null,
                  "duration": {
                    "text": "3 mins",
                    "value": 156
                  },
                  "end_location": {
                    "lng": 4.884835000000001,
                    "lat": 52.359696
                  },
                  "distance": {
                    "text": "0.2 km",
                    "value": 203
                  },
                  "steps": [
                    {
                      "polyline": {
                        "points": "}qq~H}wx\\D\\@A~@m@RI@ABAB?@?B@B?"
                      },
                      "start_location": {
                        "lng": 4.883353,
                        "lat": 52.360154
                      },
                      "duration": {
                        "text": "1 min",
                        "value": 44
                      },
                      "end_location": {
                        "lng": 4.8834965,
                        "lat": 52.3595926
                      },
                      "distance": {
                        "text": "63 m",
                        "value": 63
                      },
                      "html_instructions": "Head <b>southeast</b> on <b>Hobbemastraat</b> toward <b>Paulus Potterstraat</b>",
                      "travel_mode": "WALKING"
                    },
                    {
                      "polyline": {
                        "points": "mnq~H{xx\\@O@EBINa@DK@A?A@?BCVOBCDEBC@E@A@C"
                      },
                      "start_location": {
                        "lng": 4.8834965,
                        "lat": 52.3595926
                      },
                      "duration": {
                        "text": "1 min",
                        "value": 47
                      },
                      "maneuver": "turn-left",
                      "end_location": {
                        "lng": 4.8841404,
                        "lat": 52.3591803
                      },
                      "distance": {
                        "text": "66 m",
                        "value": 66
                      },
                      "html_instructions": "Turn <b>left</b> at <b>Paulus Potterstraat</b>",
                      "travel_mode": "WALKING"
                    },
                    {
                      "polyline": {
                        "points": "{kq~H{|x\\{A{BKO"
                      },
                      "start_location": {
                        "lng": 4.8841404,
                        "lat": 52.3591803
                      },
                      "duration": {
                        "text": "1 min",
                        "value": 65
                      },
                      "maneuver": "turn-left",
                      "end_location": {
                        "lng": 4.884835000000001,
                        "lat": 52.359696
                      },
                      "distance": {
                        "text": "74 m",
                        "value": 74
                      },
                      "html_instructions": "Turn <b>left</b> onto <b>Museumstraat</b>",
                      "travel_mode": "WALKING"
                    }
                  ],
                  "html_instructions": "Walk to Museumstraat, 1071 Amsterdam, Netherlands",
                  "travel_mode": "WALKING"
                }
              ],
              "end_location": {
                "lng": 4.884835000000001,
                "lat": 52.359696
              },
              "distance": {
                "text": "15.1 km",
                "value": 15128
              },
              "via_waypoint": [],
              "traffic_speed_entry": [],
              "end_address": "Museumstraat, 1071 Amsterdam, Netherlands",
              "start_address": "Schiphol, Netherlands",
              "duration": {
                "text": "40 mins",
                "value": 2372
              }
            }
          ],
          "overview_polyline": {
            "points": "ejg~Hkqa\\?xFd@]Ea@IPID_@n@[v@@FOLOf@IRIjA_ClCm@b@kA~AGFCNG`@?`D@pBI|@G\\M`@GZEl@Cv@FjANr@jBnGt@|CNjAFfAJxEGhACZSv@aAzCq@fAs@v@_@Vi@Pe@Hi@B{@MgAe@cBw@wAa@qBe@qCk@]MoAg@kAo@eBeA_By@qDcCmA{@gA_AsAmA}CsDa@c@_@k@aA_Bs@gAm@eAWw@OcAEqAAsAFaC\\kCd@mDLwABcAAkBQoMi@md@I{ECWk@cB{@yB_D{DmCyCw@P{@gAUKEsAoBsBoB_Cw@aA]m@gAiCc@}AYaCIcACiAEeB?oEAyF?_CCwACWJ_APmABGn@mB|@uBLBf@gBxC{GnBqEp@eBB]DiBAiAIoHGwAWqAaAkBAKYg@GLqAoBi@_AJUO[mAgBe@{@vGiOJKb@_AHQD[A_@EMQW_@m@m@y@i@_AOSi@y@Si@cAmAcCaE}AwC{@gBMWOGU?OPiArBaBrCiBnCc@Ze@L[B[Ew@_@a@a@sBmCy@qA]k@_AyByIuOcB_Du@wAmAcBgF_Ko@eBKYKKYMMAAw@Ee@_AyCoFuNe@uAGa@?MSc@K[k@}AWi@UIYECVMv@IRUVuAhAUNEJMLG@YNMAKOIIEQ_@qCOcBk@aHOoCMyKQeA?mE@Q?wA?wABmE?}ABuI@sB?yBBaF?qB@gC?aEBaS?u@@_ABuB?eABq@BkBJoBVeGf@yFl@cGr@oHT{DF}AD{B@}FGsEMsFKkEKmEEiB?Y?BGiDGkESaIIsA?mAEiDOaGj@EEiDAcBo@Aq@UIRi@A{@Dg@?c@BWYKQCGu@?q@?}@@sCOo@Ko@Ig@Ca@BuBl@o@LiARaCj@QHu@AERaAZ{Ah@S@QBQGSc@Iq@KqBCWSGU?YD_AFgB?uBFoBBq@?eCJy@@gAFQ?KLGMO?a@EEUIOQMMESBSL[y@UkAiAuGg@qD]}AkAgHe@wC}@cFiAoGyAmJ_AiH[wASo@U{@}@sFg@sCqBsLm@yDoBcKc@qCSw@o@oAS[e@qA@Yc@}AG_@KDULc@T{@j@}@l@oDzBa@ZM{@qAeH_AyFS_AIDMDm@eDc@gCEKGCS@k@Zk@JD\\@ArAw@DCL@BURk@HOd@]HOgBkC"
          },
          "waypoint_order": []
        }
      ],
      "geocoded_waypoints": [
        {
          "geocoder_status": "OK",
          "types": [
            "locality",
            "political"
          ],
          "place_id": "ChIJHU4RWyjhxUcR27TNSXntfQs"
        },
        {
          "geocoder_status": "OK",
          "types": [
            "route"
          ],
          "place_id": "ChIJc-_iku4JxkcRhBT3AzDuMg0",
          "partial_match": true
        }
      ]
    }
  },
  "uber": {
    "status_code": 200,
    "response": {
      "product_id": "98ea6c00-6408-430b-b2f0-3ff3a8b87d00",
      "localized_display_name": "uberX",
      "currency_code": "EUR",
      "surge_multiplier": 1.0,
      "high_estimate": 26,
      "display_name": "uberX",
      "minimum": 5,
      "waiting_time": 120,
      "estimate": "\u20ac19-26",
      "distance": 9.11,
      "duration": 1080,
      "low_estimate": 19
    }
  }
};
