import {Platform, ToastController} from 'ionic-angular';
import {Component, Input, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';

export interface Itinerary {
    icon: string;
    info: string;
    departureEpoch: number;
    direction: string;
}

@Component({
  selector: 'itinerary',
  templateUrl: 'itinerary.html'
})

export class ItineraryComponent {
  @Input() itinerary: Itinerary[];

  constructor(private platform: Platform) {
  }

  ngOnInit() {
  }
}


export function extractItinerary(plan): Itinerary [] {
  let modes: Itinerary[] = [];

  if (plan.hasOwnProperty('transit')) {
    let len = plan.transit.response.routes[0].legs.length;
    let bounds = plan.transit.response.routes[0].bounds;
    let legs = plan.transit.response.routes[0].legs;
    for (var leg in legs) {
      // endLocation = leg.end_location;
      for (var step in legs[leg].steps) {
        if (legs[leg].steps[step].travel_mode === "WALKING") {
          if (Math.round(legs[leg].steps[step].duration.value / 60) !== 0) {
            modes.push({
              icon: 'walk',
              info: Math.round(legs[leg].steps[step].duration.value / 60) + ' min',
              departureEpoch: null,
              direction: legs[leg].steps[step].html_instructions
            });
          }
        }
        if (legs[leg].steps[step].travel_mode === "TRANSIT") {
          let line = legs[leg].steps[step].transit_details.line;
          modes.push({
            icon: line.vehicle.name.toLowerCase(),
            info: line.short_name,
            departureEpoch: legs[leg].steps[step].transit_details.departure_time.value,
            direction: line.name
          });
        }
      }
    }
  }
  return modes;
}
